# File Storage Backend API

## Setup MongoDB Server (macos using homebrew)

- `brew tap mongodb/brew`
- `brew install mongodb-community@4.4`

### Commands (macos using homebrew)

- `brew services start mongodb-community@4.4` - Start Server.
- `ps aux | grep -v grep | grep mongod` - Verify Server Process.
- `brew services stop mongodb-community@4.4` - Stop Server.

## Setup MongoDB Server (linux ubuntu 20.04)

- `wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -`
- `echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list`
- `sudo apt update`
- `sudo apt install -y mongodb-org -y`

### Commands (MongoDB linux)

- `sudo systemctl start mongod` - Start Server.
- `sudo systemctl status mongod` - Verify Server Process.
- `sudo systemctl stop mongod` - Stop Server.
- `sudo systemctl restart mongod` - Restart Server.

## Usage
- `npm start` - Install dependencies.
- `brew services start mongodb-community@4.4` or `sudo systemctl start mongod` - Start mongodb Server.
- `npm run start`
- Open `http://127.0.0.1:3001/fsdeo/` or `http://localhost:3001/fsdeo/` in any browser

## Additional Notes
- I use swagger which is built into nestjs as a module to test the api locally.
- I was planning to use a Dockerfile to spin up a mongodb server locally but due to weird
connectivity issues and lack of time I am only able to test this locally without Docker. I apologize for
the inconvenience.
