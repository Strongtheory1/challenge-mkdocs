FROM python:3.8-slim

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN pip install mkdocs

EXPOSE 8080

# VOLUME [ "/base" ]
WORKDIR /base
COPY mkdockerize.sh /base/scripts/mkdockerize.sh
RUN chmod u+x /base/scripts/mkdockerize.sh

ENTRYPOINT ["/base/scripts/mkdockerize.sh"]

# FROM ubuntu:20.04

# RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
# RUN apt-get install -y python3 python3-pip
# RUN pip3 install mkdocs

# # RUN mkdir -p /home/base
# # COPY mkdockerize.sh /home/base/mkdockerize.sh
# # RUN chmod u+x /home/base/mkdockerize.sh

# VOLUME /base
# COPY mkdockerize.sh /base/scripts/mkdockerize.sh
# RUN chmod +x /base/scripts/mkdockerize.sh

# # ENTRYPOINT ["/home/base/mkdockerize.sh"]
# ENTRYPOINT ["/base/scripts/mkdockerize.sh"]