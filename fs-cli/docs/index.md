fs-store CLI Commands
============

- ClI for the file management system.
- Note: Make sure the local API server (fs-store-back) and mongodb server is running (http://127.0.0.1:3001/fsdeo)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage (local)
<!-- usage -->
```sh-session
$ npm install
$ npm link

USAGE
  $ fs-store COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`fs-store upload-file`](#fs-store-upload-file)
* [`fs-store list-files`](#fs-store-list-files)
* [`fs-store delete-file`](#fs-store-delete-file)
* [`fs-store get-file`](#fs-store-get-file)

## `fs-store upload-file`

- Upload a file via the upload api call.

```
USAGE
  $ fs-store upload-file -x <filename or filepath>

OPTIONS
  -h, --help       show CLI help
  -x, --file=file  (required) filename/filepath of the file to upload

CURL REQUEST
  curl -X POST "http://127.0.0.1:3001/upload" -H  "accept: */*" -H  "Content-Type: multipart/form-data" -F "file=@:file"

RESPONSE
  {
    _id
  }
```

## `fs-store list-files`

- Get all files.

```
USAGE
  $ fs-store list-files

OPTIONS
  -h, --help  show CLI help

CURL REQUEST
  curl -X GET "http://127.0.0.1:3001/files" -H  "accept: */*"

RESPONSE
  [
    {
      _id
      length
      chunkSize
      uploadDate
      filename
      md5
      contentType
    },
    ...
  ]
```

## `fs-store delete-file`

- Delete a file via the ID.
- Note: Get the file id from the the get all files command.


```
USAGE
  $ fs-store delete-file -i <ID>

OPTIONS
  -h, --help   show CLI help
  -i, --ID=ID  (required) file ID value

CURL REQUEST
  curl -X DELETE "http://127.0.0.1:3001/remove/:id" -H  "accept: */*"

RESPONSE
  NONE
```

## `fs-store get-file`

- Get information about a file via the ID.
- Note: Get the file id from the the get all files command.

```
USAGE
  $ fs-store get-file -i <ID>

OPTIONS
  -h, --help   show CLI help
  -i, --ID=ID  (required) file ID value

CURL REQUEST
  curl -X GET "http://127.0.0.1:3001/file/:id" -H  "accept: */*"

RESPONSE
  [
    {
      _id
      length
      chunkSize
      uploadDate
      filename
      md5
      contentType
    }
  ]
```
<!-- commandsstop -->
