# Project Information

## Project Tools
- OCLIF: Framework written by Heroku using typescript (using axios)

## Project layout
    src/
        commands/                   # Core
            upload-file.ts          # upload file request
            list-files.ts           # get a list of all the files (information)
            delete-file.ts          # delete a single file via the ID
            get-file.ts             # get a single file via the ID (information)
            interface/
                file.interface.ts   # dto file to match the mongodb document schema (extends document)
                dt.interface.ts     # Ignore
            schemas/
                file.schema.ts      # mongodb schema
                fsb.schema.ts       # Ignore
