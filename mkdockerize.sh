#!/usr/bin/env bash

TR="$1"  # Command Input [produce, server]
FP="$2"  # Filepath of the mkdocs directory input

# debug handling with timestamp
function debug() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
}

# check to see if the directory is valid and exists
function confirm_dir() {
    if [[ ! -d "${FP}" ]]; then
        debug "(${FP}) directory does not exist! Please input a valid path to a mkdocs directory."
        exit 1
    fi
}

# check to see if the mkdocs.yml file exists
function confirm_file() {
    if [[ ! -f mkdocs.yml ]]; then
        debug "(mkdocs.yml) file does not exist! File is required to produce the documentation."
        exit 1
    fi
}

# mkdocs defaults to 127.0.0.1
# check to see if 0.0.0.0:8000 is not already set for localhost
function check_addr() {
    if grep -q "dev_addr: 0.0.0.0:8000" mkdocs.yml; then
        :
    else
        echo "dev_addr: 0.0.0.0:8000" >> mkdocs.yml
    fi
}

# Produce Step (Create tar.gz compressed file to stdout.)
function produce() {
    cd "${FP}" || exit
    debug $(ls -a)

    confirm_file
    check_addr

    mkdocs build &> /dev/null

    cd ../

    tar czf - -C "${FP}" .
}

# Serve Step (Serve tar.gz compressed file website to port 8000.)
function serve() {
    mkdir docs
    tar xzf - -C docs

    cd docs || exit
    mkdocs serve
}

# Starting point
function handler() {
    if [[ "${TR}" == "produce" ]]; then
        confirm_dir
        produce
    elif [[ "${TR}" == "serve" ]]; then
        serve
    else
        debug "Please pass in an appropriate request. Accepted Values: [produce, serve]"
        exit 1
    fi
}

debug $(ls -a /base)
handler
